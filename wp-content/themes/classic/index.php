<?php

get_header(); ?>
	<div class="categories-bar">
		<div class="container">
			<?php wp_nav_menu( array(
				'theme_location'  => 'categories-menu',
				'container'       => false,
				'menu_class'      => 'cat-menu row end-sm center-xs'
			)); ?>
		</div>
	</div>
	<section class="post-list">
		<div class="container">
			<?php query_posts($query_string . '&cat=-9'); ?>
			<?php if (have_posts()):
				while (have_posts()): the_post(); ?>
			<article class="post">
				<h2 class="center-xs">
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				</h2>
				<span class="date center-xs"> Posted on
					<?php the_time( 'F j, Y ' ); ?>
				</span>
				<div class="img-wrap">
					<a href="<?php the_permalink(); ?>">
						<?php the_post_thumbnail('full', 'class=img-responsive'); ?>
					</a>
				</div>
				<?php the_excerpt(); ?>
				<a class="button" href="<?php the_permalink(); ?>">Read more</a>
			</article>

				<?php endwhile;
			else :
				echo "No content";
			endif;?>
			<div class="pag-wrap center-xs">
				<?php
				global $wp_query;

				$big = 999999999; // need an unlikely integer

				echo paginate_links( array(
					'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
					'format' => '?paged=%#%',
					'total' => $wp_query->max_num_pages,
					'prev_text' => '',
					'next_text' => ''
				) );
				?>
			</div>
	</section>



<?php get_footer();

?>