<?php

function load_style() {
    wp_enqueue_style('bootstrap.min.css', get_template_directory_uri() . '/styles/css/bootstrap.min.css');
    wp_enqueue_style('flexboxgrid.min.css', get_template_directory_uri() . '/styles/flexboxgrid/css/flexboxgrid.min.css');
    wp_enqueue_style('font-awesome.min.css', get_template_directory_uri() . '/styles/font-awesome/css/font-awesome.min.css');
    wp_enqueue_style('hover.css', get_template_directory_uri() . '/styles/css/hover.css');
    wp_enqueue_style('main', get_template_directory_uri() . '/styles/css/styles.css');
}

add_action ('wp_enqueue_scripts', 'load_style');

function load_script() {
    wp_enqueue_script('jquery-1.12.0', get_template_directory_uri() . '/js/jquery-1.12.0.min.js');
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js');
    wp_enqueue_script('main', get_template_directory_uri() . '/js/main.js');
}

add_action ('wp_enqueue_scripts', 'load_script');

// Navigation Menus
register_nav_menu ('main-menu', 'header-menu');

// Categories Menus
register_nav_menu ('categories-menu', 'cat-menu');

// Footer Menus
register_nav_menu ('footer-menu', 'foot-menu');

// LOGO
function my_after_setup_theme() {
    add_image_size( 'my-theme-logo-size', auto, auto, true );
    add_theme_support( 'site-logo', array( 'size' => 'my-theme-logo-size' ) );
}
add_action( 'after_setup_theme', 'my_after_setup_theme' );

// поддержка миниаютр
add_theme_support('post-thumbnails');

/**
 * Register our sidebars and widgetized areas.
 *
 */
register_sidebar( array(
    'name'          => 'sidebar',
    'id'            => 'footer-sidebar',
    'before_title'  => '<h2>',
    'after_title'   => '</h2>',
    'description'   => 'create widgets here'
) );


function is_type_page() { // Check if the current post is a page
    global $post;

    if ($post->post_type == 'page') {
        return true;
    } else {
        return false;
    }
}



// Creates Movie Reviews Custom Post Type
function team_reviews_init() {
	$args = array(
		'label' => 'Team',
		'public' => true,
		'show_ui' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'rewrite' => array('slug' => 'team-reviews'),
		'query_var' => true,
		'supports' => array(
			'title',
			'editor',
			'excerpt',
			'trackbacks',
			'custom-fields',
			'comments',
			'revisions',
			'thumbnail',
			'author',
			'page-attributes',)
	);
	register_post_type( 'team-reviews', $args );
}
add_action( 'init', 'team_reviews_init' );

?>