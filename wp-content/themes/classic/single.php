<?php

get_header(); ?>
    <div class="categories-bar">
        <div class="container">
            <?php wp_nav_menu( array(
                'theme_location'  => 'categories-menu',
                'container'       => false,
                'menu_class'      => 'cat-menu row end-sm center-xs'
            ));?>
        </div>
    </div>
    <section class="post-list">
        <div class="container">
        <?php if (have_posts()):
            while (have_posts()): the_post(); ?>
            <article class="post">
                <h2 class="center-xs">
                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                </h2>
                <span class="date center-xs"> Posted on
                    <?php the_time( 'F j, Y ' ); ?>
                </span>
                <div class="img-wrap">
                    <a href="<?php the_permalink(); ?>">
                        <?php the_post_thumbnail('full', 'class=img-responsive'); ?>
                    </a>
                </div>
                <?php the_content(); ?>
                <?php echo do_shortcode("[simple-social-share]"); ?>
                <?php comments_template(); ?>
            </article>

        <?php endwhile;
            else :
                echo "No content";
        endif;?>


<?php get_footer();

?>