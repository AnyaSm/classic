<?php
/*
Template Name: About
*/
get_header(); ?>
    <section class="about">
        <div class="container">
            <div class="about-us">

                <?php
                query_posts('p=54');
                while ( have_posts() ) : the_post(); ?>
                    <h2 class="center-xs">
                        <?php the_title(); ?>
                    </h2>
                    <?php the_content(); ?>
                <?php endwhile; ?>

            </div>
            <div class="team">
                <h2 class="center-xs">Meet Our Team!</h2>
                <?php
                $query = new WP_Query( array('post_type' => 'team-reviews', 'posts_per_page' => 100 ) );
                if ($query->have_posts()):?>
                <ul class="team-list row center-xs">
                <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                    <li class="col-sm-4">
                        <div class="img-wrap">
                            <?php the_post_thumbnail('full', 'class=img-responsive'); ?>
                        </div>
                        <h3><?php the_title(); ?></h3>
                        <div class="description">
                            <?php the_content(); ?>
                        </div>
                    </li>
                <?php endwhile; ?>
                </ul>
                <?php endif; wp_reset_postdata(); ?>
            </div>
        </div>
    </section>

<?php get_footer();

?>
