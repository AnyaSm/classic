        <div class="container">
            <footer class="footer">
                <?php wp_nav_menu( array(
                    'theme_location'  => 'footer-menu',
                    'container'       => false,
                    'menu_class'      => 'foot-menu row col-sm-4 col-xs-12 start-sm center-xs'
                )); ?>
                <!-- social icon widget -->
                <div id="footer-sidebar" class="secondary">
                    <ul>
                        <?php if(!dynamic_sidebar('footer-sidebar')) : ?>

                        <?php endif; ?>
                    </ul>
            </footer>
        </div>

<?php wp_footer(); ?>

</body>
</html>